'use strict';

const UserModel = require('../models/users.model');
const ActivationModel = require('../models/activation.model');
const ResetPasswordModel = require('../models/reset-password.model');
const LoginHistoryModel = require('../models/login-history.model');
const crypto = require('crypto');
const uuidv1 = require('uuid/v1');
const helpers = require('../../common/helpers');
const fs = require('fs');
const constants = require('../../common/constants');

// Create user with registration form
exports.createUser = (req, res, next) => {
  const errors = [];

  if (req.body.password.length < constants.PASSWORD_MIN_LENGTH) {
    errors.push(`Minimum required password length is ${constants.PASSWORD_MIN_LENGTH}`);
  }

  if (!helpers.validateEmail(req.body.email)) {
    errors.push('Invalid email');
  }

  if (errors.length) {
    res.status(422).send({ errors: errors });

    return next();
  }

  const salt = crypto.randomBytes(16).toString('base64');
  const hash = crypto.createHmac('sha512', salt).update(req.body.password).digest('base64');
  const name = req.body.name;
  const email = req.body.email;
  const password = salt + '$' + hash;
  const token = uuidv1();
  const language = req.headers['content-language'];

  UserModel.create([name, email, password])
    .then(() => {
      return ActivationModel.create([token, email]);
    })
    .then(() => {
      const source = fs.readFileSync(`./common/templates/${language}/activate-user.hbs`, 'utf8');
      const subject = require(`../../common/templates/${language}/strings`).SUBJECTS.ACTIVATION_LINK;
      const activationLink = `${constants.FRONTEND_URL}/${constants.FRONTEND_ROUTES.activateUser}/${token}`;

      helpers.sendEmail(source, { activationLink: activationLink, email: email }, subject);
      res.status(201).send();
    })
    .catch(() => {
      res.status(409).send({ errors: ['User creation failed'] });
    });
};

// Add new user
exports.addNewUser = (req, res, next) => {
  let errors = [];

  if (!helpers.validateEmail(req.body.email)) {
    errors.push('Invalid email');
  }

  if (errors.length) {
    res.status(422).send({ errors: errors });

    return next();
  }

  const randomPassword = Math.random().toString(36).substring(2);
  const salt = crypto.randomBytes(16).toString('base64');
  const hash = crypto.createHmac('sha512', salt).update(randomPassword).digest('base64');
  const name = req.body.name;
  const email = req.body.email;
  const password = salt + '$' + hash;
  const token = uuidv1();
  const language = req.headers['content-language'];

  UserModel.create([name, email, password])
    .then(() => {
      return ActivationModel.create([token, email]);
    })
    .then(() => {
      const source = fs.readFileSync(`./common/templates/${language}/new-user.hbs`, 'utf8');
      const subject = require(`../../common/templates/${language}/strings`).SUBJECTS.ACTIVATION_LINK;
      const activationLink = `${constants.FRONTEND_URL}/${constants.FRONTEND_ROUTES.activateUser}/${token}`;
      const data = {
        activationLink: activationLink,
        password: randomPassword,
        email: email,
      };

      helpers.sendEmail(source, data, subject);

      return UserModel.findByEmail(email);
    })
    .then((result) => {
      const user = { ...result };

      delete user.password;

      res.status(201).send(user);
    })
    .catch(() => {
      res.status(409).send({ errors: ['User creation failed'] });
    });
};

exports.findUserByEmail = (req, res) => {
  UserModel.findByEmail(req.body.email)
    .then((user) => {
      res.status(200).send(user);
    })
    .catch(() => {
      res.status(404).send();
    });
};

exports.deleteUserByEmail = (req, res) => {
  UserModel.deleteByEmail(req.body.email)
    .then(() => {
      const language = req.headers['content-language'];
      const source = fs.readFileSync(`./common/templates/${language}/delete-user.hbs`, 'utf8');
      const subject = require(`../../common/templates/${language}/strings`).SUBJECTS.DELETE_USER;

      helpers.sendEmail(source, { email: req.body.email }, subject);
      res.status(200).send({ email: req.body.email });
    })
    .catch((err) => {
      console.log(err);
      res.status(404).send();
    });
};

exports.activateUser = (req, res) => {
  ActivationModel.findByToken(req.params.token)
    .then((activation) => {
      return ActivationModel.activateUser(activation.email);
    })
    .then(() => {
      res.status(200).send();
    })
    .catch(() => {
      res.status(404).send();
    });
};

exports.editUser = (req, res) => {
  UserModel.editUser(req.body.name, req.body.email)
    .then(() => {
      return UserModel.findByEmail(req.body.email);
    })
    .then((result) => {
      const user = { ...result };

      delete user.password;

      res.status(201).send(user);
    })
    .catch(() => {
      res.status(404).send();
    });
};

exports.requestUserPasswordReset = (req, res) => {
  const email = req.body.email;
  const token = uuidv1();
  const language = req.headers['content-language'];

  ResetPasswordModel.create([email, token])
    .then(() => {
      const source = fs.readFileSync(`./common/templates/${language}/password-reset-link.hbs`, 'utf8');
      const subject = require(`../../common/templates/${language}/strings`).SUBJECTS.PASSWORD_RESET;
      const passwordResetLink = `${constants.FRONTEND_URL}/${constants.FRONTEND_ROUTES.resetPassword}/${token}`;

      helpers.sendEmail(source, { passwordResetLink: passwordResetLink, email: email }, subject);
      res.status(200).send();
    })
    .catch(() => {
      res.status(409).send();
    });
};

exports.resetUserPasswordWithToken = (req, res, next) => {
  const salt = crypto.randomBytes(16).toString('base64');
  const hash = crypto.createHmac('sha512', salt).update(req.body.password).digest('base64');
  const email = req.body.email;
  const password = salt + '$' + hash;
  const token = req.params.token;

  ResetPasswordModel.findByToken(token)
    .then((result) => {
      if (!result) {
        res.status(404).send();

        return next();
      }

      return ResetPasswordModel.resetPassword([email, password]);
    })
    .then(() => {
      const language = req.headers['content-language'];
      const source = fs.readFileSync(`./common/templates/${language}/password-reset-notification.hbs`, 'utf8');
      const subject = require(`../../common/templates/${language}/strings`).SUBJECTS.PASSWORD_RESET_NOTIFICATION;

      helpers.sendEmail(source, { email: email }, subject);
      res.status(200).send();
    })
    .catch(() => {
      res.status(404).send();
    });
};

exports.getAllUsers = (req, res) => {
  let count;

  UserModel.countUsers()
    .then((result) => {
      count = result;

      return UserModel.get(req.body);
    })
    .then((result) => {
      const users = [...result];

      users.forEach((user) => delete user.password);

      res.status(200).send({ users: result, total: count['COUNT(*)'] });
    })
    .catch(() => {
      res.status(500).send();
    });
};

exports.getAllLoginHistory = (req, res) => {
  LoginHistoryModel.get()
    .then((result) => {
      res.status(200).send(result);
    })
    .catch(() => {
      res.status(500).send();
    });
};

exports.getLoginHistoryByEmail = (req, res) => {
  let count;

  LoginHistoryModel.countLoginHistoryByEmail(req.body.email)
    .then((result) => {
      count = result;

      return LoginHistoryModel.getLoginHistoryByEmail(req.body.email, req.body.pagination);
    })
    .then((result) => {
      res.status(200).send({ loginHistory: result, total: count['COUNT(*)'] });
    })
    .catch(() => {
      res.status(404).send();
    });
};
