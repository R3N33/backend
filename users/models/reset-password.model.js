'use strict';

const sqlite3 = require('sqlite3').verbose();
const databaseFilename = require('../../common/config/env.config').databaseFilename;
const database = new sqlite3.Database(`./${databaseFilename}`);

exports.createResetPasswordTable = () => {
  const sql = `
	CREATE TABLE IF NOT EXISTS resetPassword (
    id INTEGER PRIMARY KEY,
    token TEXT,
    email TEXT UNIQUE)
  `;

  database.run(sql);
};

exports.create = ([email, token], cb) => {
  const sql = 'INSERT INTO resetPassword (email, token) VALUES (?, ?)';

  return new Promise((resolve, reject) => {
    database.get(sql, [email, token], function(err) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

exports.resetPassword = ([email, password], cb) => {
  const deleteQuery = 'DELETE FROM resetPassword WHERE email = ?';
  const updateQuery = 'UPDATE users SET password = ? WHERE email = ?';

  return new Promise((resolve) => {
    database.run(deleteQuery, email);
    database.run(updateQuery, [password, email]);

    resolve();
  });
};

exports.findByToken = (token, cb) => {
  const sql = 'SELECT * FROM resetPassword WHERE token = ?';

  return new Promise((resolve, reject) => {
    database.get(sql, token, function(err, row) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
};

exports.findByEmail = (email, cb) => {
  const sql = 'SELECT * FROM resetPassword WHERE email = ?';

  return new Promise((resolve, reject) => {
    database.get(sql, email, function(err, row) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
};
