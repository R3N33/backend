'use strict';

const sqlite3 = require('sqlite3').verbose();
const databaseFilename = require('../../common/config/env.config').databaseFilename;
const database = new sqlite3.Database(`./${databaseFilename}`);

exports.createLoginHistoryTable = () => {
  const sql = `
	CREATE TABLE IF NOT EXISTS loginHistory (
    id INTEGER PRIMARY KEY,
    email TEXT,
    datetime TEXT)
  `;

  database.run(sql);
};

exports.create = (email) => {
  const sql = 'INSERT INTO loginHistory (email, datetime) VALUES (?, CURRENT_TIMESTAMP)';

  database.run(sql, email);
};

exports.getLoginHistoryByEmail = (email, pagination) => {
  const sql = 'SELECT * FROM loginHistory WHERE email = ? LIMIT ? OFFSET ?';

  return new Promise((resolve, reject) => {
    database.all(sql, [email, pagination.limit, pagination.offset], function(err, allRows) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(allRows);
      }
    });
  });
};

exports.countLoginHistoryByEmail = (email) => {
  const sql = 'SELECT COUNT(*) FROM loginHistory WHERE email = ? ';

  return new Promise((resolve, reject) => {
    database.get(sql, [email], function(err, row) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
};

exports.get = () => {
  const sql = 'SELECT * FROM loginHistory';

  return new Promise((resolve, reject) => {
    database.get(sql, function(err, allRows) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(allRows);
      }
    });
  });
};
