'use strict';

const sqlite3 = require('sqlite3').verbose();
const databaseFilename = require('../../common/config/env.config').databaseFilename;
const database = new sqlite3.Database(`./${databaseFilename}`);

exports.createActivationTable = () => {
  const sql = `
	CREATE TABLE IF NOT EXISTS activation (
    id INTEGER PRIMARY KEY,
    token TEXT,
    email TEXT UNIQUE)
  `;

  database.run(sql);
};

exports.create = (activation, cb) => {
  const sql = 'INSERT INTO activation (token, email) VALUES (?, ?)';

  return new Promise((resolve, reject) => {
    database.run(sql, activation, function(err) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

exports.activateUser = (email, cb) => {
  const deleteQuery = 'DELETE FROM activation WHERE email = ?';
  const updateQuery = 'UPDATE users SET isActivated = 1 WHERE email = ?';

  return new Promise((resolve) => {
    database.run(deleteQuery, email);
    database.run(updateQuery, email);

    resolve();
  });
};

exports.findByToken = (token, cb) => {
  const sql = 'SELECT * FROM activation WHERE token = ?';

  return new Promise((resolve, reject) => {
    database.get(sql, token, function(err, row) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
};

exports.findByEmail = (email, cb) => {
  const sql = 'SELECT * FROM activation WHERE email = ?';

  return new Promise((resolve, reject) => {
    database.get(sql, email, function(err, row) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
};
