'use strict';

const sqlite3 = require('sqlite3').verbose();
const databaseFilename = require('../../common/config/env.config').databaseFilename;
const database = new sqlite3.Database(`./${databaseFilename}`);

exports.createUsersTable = () => {
  const sql = `
    CREATE TABLE IF NOT EXISTS users (
    id INTEGER PRIMARY KEY,
    name TEXT,
    email TEXT UNIQUE,
		password TEXT,
    isActivated INTEGER DEFAULT 0)
  `;

  database.run(sql);
};

exports.findByEmail = (email) => {
  const sql = 'SELECT * FROM users WHERE email = ?';

  return new Promise((resolve, reject) => {
    database.get(sql, [email], function(err, row) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
};

exports.editUser = (name, email) => {
  const updateSql = 'UPDATE users SET name = ? WHERE email = ?';

  return new Promise((resolve, reject) => {
    database.run(updateSql, [name, email], function(err) {
      if (err) {
        console.log('Error running sql ' + updateSql);
        console.log(err);
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

exports.create = (user) => {
  const sql = 'INSERT INTO users (name, email, password) VALUES (?, ?, ?)';

  return new Promise((resolve, reject) => {
    database.run(sql, user, function(err) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

exports.deleteByEmail = (email) => {
  const deleteUserSql = 'DELETE FROM users WHERE email = ?';
  const deleteActivationTokenSql = 'DELETE FROM activation WHERE email = ?';
  const deleteLoginHistory = 'DELETE FROM loginHistory WHERE email = ?';

  return new Promise((resolve, reject) => {
    database.run(deleteUserSql, email);
    database.run(deleteActivationTokenSql, email);
    database.run(deleteLoginHistory, email);

    resolve();
  });
};

exports.get = (pagination) => {
  const sql = 'SELECT * FROM users LIMIT ? OFFSET ?';

  return new Promise((resolve, reject) => {
    database.all(sql, [pagination.limit, pagination.offset], function(err, allRows) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(allRows);
      }
    });
  });
};

exports.countUsers = () => {
  const sql = 'SELECT COUNT(*) FROM users ';

  return new Promise((resolve, reject) => {
    database.get(sql, function(err, row) {
      if (err) {
        console.log('Error running sql ' + sql);
        console.log(err);
        reject(err);
      } else {
        resolve(row);
      }
    });
  });
};
