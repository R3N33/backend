'use strict';

const UsersController = require('./controllers/users.controller');
const ValidationMiddleware = require('../common/middlewares/auth.validation.middleware');
const constants = require('../common/constants');

exports.routesConfig = function(app) {
  /**
   * Creates a new user
   * @route POST /users/register
   * @group Users
   * @param {NewUser.model} user.body.required - New user
   * @returns 201 - If user creation was successful
   * @returns {Error} 422 - { errors: [] }
   * @returns {Error} 409 - { errors: [] }
   * @returns {Error} 401 - When unauthorized
  */
  app.post(`/${constants.API_ENDPOINTS.registerUser}`, [
    UsersController.createUser,
  ]);

  /**
   * Get all users
   * @route POST /users
   * @group Users
   * @param {Pagination.model} pagination.body.required - Pagination
   * @returns {GetAllUsersResponse.model} 200 - List of users with total row count
   * @returns {Error} 500 - When getting users fails
   * @security JWT
  */
  app.post(`/${constants.API_ENDPOINTS.users}`, [
    ValidationMiddleware.validJWTNeeded,
    UsersController.getAllUsers,
  ]);

  /**
   * Activates user with token
   * @route GET /users/activate/:token
   * @group Users
   * @param {string} token.params.required - Token
   * @returns 200 - If activation was successful
   * @returns {Error} 404 - When token is not found
  */
  app.get(`/${constants.API_ENDPOINTS.activateUser}/:token`, [
    UsersController.activateUser,
  ]);

  /**
   * Request to reset password
   * @route POST /users/reset-password
   * @group Users
   * @param {Email.model} email.body.required - E-mail
   * @returns 200 - If password request was successful
   * @returns {Error} 409 - When password reset request fails
  */
  app.post(`/${constants.API_ENDPOINTS.resetPassword}`, [
    UsersController.requestUserPasswordReset,
  ]);

  /**
   * Resets user password
   * @route POST /users/reset-password/:token
   * @group Users
   * @param {string} token.params.required - Token
   * @param {Credentials.model} credentials.body.required - Credentials
   * @returns 200 - If password reset was successful
   * @returns {Error} 404 - When password reset fails
  */
  app.post(`/${constants.API_ENDPOINTS.resetPassword}/:token`, [
    UsersController.resetUserPasswordWithToken,
  ]);

  /**
   * Delete user
   * @route POST /users/delete
   * @group Users
   * @param {Email.model} email.body.required - E-mail
   * @returns {Email.model} 200 - If delete was successful
   * @returns {Error} 404 - When delete fails
   * @security JWT
  */
  app.post(`/${constants.API_ENDPOINTS.deleteUser}`, [
    ValidationMiddleware.validJWTNeeded,
    UsersController.deleteUserByEmail,
  ]);

  /**
   * Add new user
   * @route POST /users/new
   * @group Users
   * @param {Credentials.model} credentials.body.required - Credentials
   * @returns {User} 201 - If user creation was successful
   * @returns {Error} 422 - { errors: [] }
   * @returns {Error} 409 - { errors: [] }
   * @security JWT
  */
  app.post(`/${constants.API_ENDPOINTS.addNewUser}`, [
    ValidationMiddleware.validJWTNeeded,
    UsersController.addNewUser,
  ]);

  /**
   * Get every users login history
   * @route GET /users/history
   * @group Users
   * @returns {Array.<LoginHistory>} 200 - If fetching users login history was successful
   * @returns {Error} 500 - When getting login history fails
   * @security JWT
  */
  app.get(`/${constants.API_ENDPOINTS.userLoginHistory}`, [
    ValidationMiddleware.validJWTNeeded,
    UsersController.getAllLoginHistory,
  ]);

  /**
   * Get specific users login history
   * @route POST /users/history
   * @group Users
   * @param {LoginHistoryRequest.model} loginHistoryRequest.body.required - Login history request
   * @returns {LoginHistoryResponse.model} 200 - If getting login history was successful
   * @returns {Error} 404 - When getting login history fails
   * @security JWT
  */
  app.post(`/${constants.API_ENDPOINTS.userLoginHistory}`, [
    ValidationMiddleware.validJWTNeeded,
    UsersController.getLoginHistoryByEmail,
  ]);

  /**
   * Edit user
   * @route PUT /users/edit
   * @group Users
   * @param {EditUserRequest.model} editUserRequest.body.required - Edit user request
   * @returns {User.model} 201 - If user edit was successful
   * @returns {Error} 404 - When editing user fails
   * @security JWT
  */
  app.put(`/${constants.API_ENDPOINTS.editUser}`, [
    ValidationMiddleware.validJWTNeeded,
    UsersController.editUser,
  ]);
};
