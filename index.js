'use strict';

const config = require('./common/config/env.config.js');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const AuthorizationRouter = require('./authorization/routes.config');
const UsersRouter = require('./users/routes.config');
const database = require('./common/database/database');

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
  res.header('Access-Control-Expose-Headers', 'Content-Length');
  res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');

  if (req.method === 'OPTIONS') {
    return res.send(200);
  } else {
    return next();
  }
});

app.use(bodyParser.json());
AuthorizationRouter.routesConfig(app);
UsersRouter.routesConfig(app);
database.createDatabase();
app.listen(config.port, function() {
  console.log('App listening at port %s', config.port);
});

const expressSwagger = require('express-swagger-generator')(app);
let options = {
  swaggerDefinition: {
    info: {
      description: 'Swagger documentation for backend',
      title: 'Swagger',
      version: '1.0.0',
    },
    host: 'localhost:3600',
    basePath: '',
    produces: [
      'application/json',
      'application/xml',
    ],
    schemes: ['http', 'https'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description: '',
      },
    },
  },
  basedir: __dirname,
  files: [
    './authorization/routes.config.js',
    './users/routes.config.js',
    './common/swagger-models.js',
  ],
};

expressSwagger(options);
