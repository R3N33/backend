'use strict';

const UserModel = require('../../users/models/users.model');
const crypto = require('crypto');

exports.hasAuthValidFields = (req, res, next) => {
  let errors = [];

  if (req.body) {
    if (!req.body.email) {
      errors.push('Missing email field');
    }
    if (!req.body.password) {
      errors.push('Missing password field');
    }

    if (errors.length) {
      return res.status(400).send({errors: errors.join(',')});
    } else {
      return next();
    }
  } else {
    return res.status(400).send({ errors: 'Missing email and password fields' });
  }
};

exports.isPasswordAndUserMatch = (req, res, next) => {
  UserModel.findByEmail(req.body.email)
    .then((user) => {
      if (!user) {
        res.status(404).send();
      } else {
        const passwordFields = user.password.split('$');
        const salt = passwordFields[0];
        const hash = crypto
          .createHmac('sha512', salt)
          .update(req.body.password)
          .digest('base64');

        if (hash === passwordFields[1]) {
          req.body = {
            id: user.id,
            isActivated: user.isActivated,
            email: user.email,
            name: user.name,
          };

          return next();
        } else {
          return res.status(400).send({errors: ['Invalid e-mail or password']});
        }
      }
    });
};

exports.isActivated = (req, res, next) => {
  UserModel.findByEmail(req.body.email)
    .then((user) => {
      if (!user) {
        res.status(404).send();
      } else if (user.isActivated) {
        return next();
      } else {
        res.status(400).send({ errors: ['User is not activated'] });
      }
    });
};
