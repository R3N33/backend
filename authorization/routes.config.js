'use strict';

const VerifyUserMiddleware = require('./middlewares/verify.user.middleware');
const AuthorizationController = require('./controllers/authorization.controller');
const constants = require('../common/constants');

exports.routesConfig = function(app) {
  /**
   * Login
   * @route POST /auth
   * @group Auth
   * @param {Credentials.model} credentials.body.required
   * @returns {string} 201 - accessToken
   * @returns {Error} 400 - { errors: [] }
   * @returns {Error} 404 - { errors: [] }
  */
  app.post(`/${constants.API_ENDPOINTS.auth}`, [
    VerifyUserMiddleware.hasAuthValidFields,
    VerifyUserMiddleware.isPasswordAndUserMatch,
    VerifyUserMiddleware.isActivated,
    AuthorizationController.login,
  ]);
};
