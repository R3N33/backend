'use strict';

const jwtSecret = require('../../common/config/env.config.js').jwt_secret;
const jwt = require('jsonwebtoken');
const LoginHistoryModel = require('../../users/models/login-history.model');
const constants = require('../../common/constants');

exports.login = (req, res) => {
  try {
    const token = jwt.sign(req.body, jwtSecret, { expiresIn: constants.JWT_EXPIRATION });

    LoginHistoryModel.create(req.body.email);

    res.status(201).send({ accessToken: token });
  } catch (err) {
    res.status(500).send({ errors: err });
  }
};
