# Backend

This project uses Node.js with Express.js

## Getting started

To get the Node server running locally:

- Clone this repo
- Install Node.js
- Install node package manager
- `npm install` to install all required dependencies
- `npm run start` to start the local server

## API Documentation

- Visit `localhost:3600/api-docs#/`
- To authorize: 
	- use the `Auth` request 
	- copy the `accessToken` from the response
	- click the `Authorize` button in the right upper corner
	- paste the `accessToken` with prefix `Bearer`... i.e `Bearer asdf.asdf.asdf`
	- click `Authorize`
	- now the requests that require authorization will have the JWT you've specified

## Scripts

- `npm run start` to start the local server
- `npm run lint` to lint the project
- `npm run lint-fix` to lint the project and fix linter errors if possible

## Postmark API

- Use your API key in `constants.js` (`POSTMARK_API_KEY`)
