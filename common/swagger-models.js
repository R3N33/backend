/**
 * @typedef NewUser
 * @property {string} email.required
 * @property {string} name.required
 * @property {string} password.required
*/

/**
 * @typedef User
 * @property {integer} id
 * @property {string} name
 * @property {string} email
 * @property {integer} isActivated
*/

/**
 * @typedef Pagination
 * @property {integer} limit.required
 * @property {integer} offset.required
*/

/**
 * @typedef GetAllUsersResponse
 * @property {Array.<User>} users
 * @property {integer} total
*/

/**
 * @typedef Credentials
 * @property {string} email.required
 * @property {string} password.required
*/

/**
 * @typedef Email
 * @property {string} email.required
*/

/**
 * @typedef LoginHistory
 * @property {integer} id.required
 * @property {string} email.required
 * @property {string} datetime.required
*/

/**
 * @typedef LoginHistoryRequest
 * @property {string} email.required
 * @property {Pagination.model} pagination.required
*/

/**
 * @typedef LoginHistoryResponse
 * @property {Array.<LoginHistory>} loginHistory
 * @property {integer} total
*/

/**
 * @typedef EditUserRequest
 * @property {string} name.required
 * @property {string} email.required
*/
