'use strict';

const UsersModel = require('../../users/models/users.model');
const ActivationModel = require('../../users/models/activation.model');
const LoginHistoryModel = require('../../users/models/login-history.model');
const ResetPasswordModel = require('../../users/models/reset-password.model');

exports.createDatabase = () => {
  UsersModel.createUsersTable();
  ActivationModel.createActivationTable();
  LoginHistoryModel.createLoginHistoryTable();
  ResetPasswordModel.createResetPasswordTable();
};
