'use strict';

const constants = require('./constants');
const Handlebars = require('handlebars');
const postmark = require('postmark');
const emailClient = new postmark.ServerClient(constants.POSTMARK_API_KEY);

exports.validateEmail = (email) => {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return re.test(String(email).toLowerCase());
};

exports.sendEmail = (templateSource, data, subject) => {
  const template = Handlebars.compile(templateSource);

  emailClient.sendEmail({
    From: constants.EMAIL_FROM,
    To: data.email,
    Subject: subject,
    HtmlBody: template(data),
  });
};
