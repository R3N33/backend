'use strict';

exports.SUBJECTS = {
  PASSWORD_RESET_NOTIFITACTON: 'Sinu parool on taastatud',
  ACTIVATION_LINK: 'Aktiveerimise link',
  PASSWORD_RESET: 'Parooli taastamine',
  DELETE_USER: 'Sinu kasutaja on kustutatud',
};
