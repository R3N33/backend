'use strict';

exports.SUBJECTS = {
  PASSWORD_RESET_NOTIFICATION: 'Your password has been reset',
  ACTIVATION_LINK: 'Activation link',
  PASSWORD_RESET: 'Password reset',
  DELETE_USER: 'Your user has been deleted',
};
