'use strict';

exports.API_URL = 'http://localhost:3600';

exports.API_ENDPOINTS = {
  users: 'users',
  auth: 'auth',
  activateUser: 'users/activate',
  deleteUser: 'users/delete',
  addNewUser: 'users/new',
  registerUser: 'users/register',
  userLoginHistory: 'users/history',
  resetPassword: 'users/reset-password',
  editUser: 'users/edit',
};

exports.FRONTEND_URL = 'http://localhost:4200/#';

exports.FRONTEND_ROUTES = {
  users: 'users',
  login: 'login',
  auth: 'auth',
  resetPassword: 'reset-password',
  registration: 'registration',
  usersInfo: 'users/info',
  activateUser: 'users/activate',
};

exports.EMAIL_FROM = 'escaper@escaper.ee';

exports.JWT_EXPIRATION = '7h';

exports.POSTMARK_API_KEY = 'YOUR_API_KEY';

exports.PASSWORD_MIN_LENGTH = 8;

exports.LANGUAGE = {
  EN: 'en-EN',
  ET: 'et-ET',
};
